package edu.csui.stopwatch

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog




class StopwatchActivity : Activity() {
    //Number of seconds displayed on the stopwatch.
    private var seconds = 0
    //Is the stopwatch running?
    private var running: Boolean = false
    private var wasRunning: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_stopwatch)
        if (savedInstanceState != null) {
            seconds = savedInstanceState.getInt("seconds")
            running = savedInstanceState.getBoolean("running")
            wasRunning = savedInstanceState.getBoolean("wasRunning")
        }
        runTimer()
    }

    override fun onPause() {
        super.onPause()
        wasRunning = running
        running = true // This line of code overrides the onPause (the default is running = false)
    }

    override fun onResume() {
        super.onResume()
        if (wasRunning) {
            running = true
        }
    }

    // Override Back Button
    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setMessage("Press the exit button on app")
            .show()
    }

    public override fun onSaveInstanceState(savedInstanceState: Bundle) {
        savedInstanceState.putInt("seconds", seconds)
        savedInstanceState.putBoolean("running", running)
        savedInstanceState.putBoolean("wasRunning", wasRunning)
    }

    //Start the stopwatch running when the Start button is clicked.
    fun onClickStart(view: View) {
        running = true
    }

    //Stop the stopwatch running when the Stop button is clicked.
    fun onClickStop(view: View) {
        running = false
    }

    //Reset the stopwatch when the Reset button is clicked.
    fun onClickReset(view: View) {
        running = false
        seconds = 0
    }

    //Exit the stopwatch app when the Exit button is clicked.
    fun onClickDestroy(view: View) {
        running = false
        seconds = 0
        super@StopwatchActivity.onBackPressed()
    }

    //Sets the number of seconds on the timer.
    private fun runTimer() {
        val timeView = findViewById(R.id.time_view) as TextView
        val handler = Handler()
        handler.post(object : Runnable {
            override fun run() {
                val hours = seconds / 3600
                val minutes = seconds % 3600 / 60
                val secs = seconds % 60
                val time = String.format(
                    "%d:%02d:%02d",
                    hours, minutes, secs
                )
                timeView.text = time
                if (running) {
                    seconds++
                }
                handler.postDelayed(this, 1000)
            }
        })
    }
}